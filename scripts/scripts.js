// ========== p5 =============

let isPlaying = true; // Initial state is playing

let updateTimeInterval = 15000; 
let seed;

// PATTERN
let periodicMotions = [];
let patternX = 20;
let patternY = 30;

let movieData;

class PeriodicMotion {
    constructor(x, y) {
        this.angle = 270;
        this.speed = 0.07;
        this.radius = 1000;
        this.sx = 4;
        this.sy = 1;
        this.x1 = x;
        this.y1 = y;
        this.x2 = 0;
        this.y2 = 0;
    }

    update() {
        this.x2 = this.x1 + cos(radians(this.angle * this.sx)) * this.radius / 2;
        this.y2 = this.y1 + sin(radians(this.angle * this.sy)) * this.radius / 2;
        this.angle += this.speed;
    }
    display(movieObject) {
        // Check if movieObject is defined before accessing its properties
        if (movieObject) {
            let userRating = movieObject['User Rating'];

            let ratingPercentage = map(userRating, 0, 10, 0, 100);
            
            let startRotation = 0; 
            let endRotation = radians(movieObject['Duration']); 

            let interpolatedRotation = lerp(startRotation, 
                                            endRotation,
                                            ratingPercentage);

            push();
            translate(this.x2, this.y2);
            rotate(interpolatedRotation);
            rect(0, 0, 500, 50); 
            pop();

            push(); // base points
            translate(this.x1, this.y1);
            rotate(interpolatedRotation);
            //rect(0, 0, 10, 10); 
            pop();
        }
    }
}

function extractNumbers(str) {
    return str.match(/\d+\.?\d*/g);
}


// function mouseClicked() {
//     console.log('===================')
//     TODO: perform a bg change
// }

function setup() {
    createCanvas(windowWidth, windowHeight);
    frameRate(60);
    fill("black");
    strokeWeight(3);
    stroke("white");
    textSize(20);
    textAlign(LEFT);

    movieData = loadTable("movies/movie_list.csv", "csv", "header", function (data) {
        headers = movieData.columns;


        // creates instances of based of window size
        for (let x = 0; x < width; x += patternX) {
            for (let y = 0; y < height; y += patternY) {
                periodicMotions.push(new PeriodicMotion(x, y));
            }
        }
    });
}

function draw() {
    push();
    console.log('===========================');
    console.log('Frame rate: ' + round(frameRate()));
    background(235, 5);
    
    if (!movieData) {
        return;
    }
    
    scale(0.25);
    translate(windowWidth*2, windowHeight*2);

    // PATTERN
    for (let i = 0; i < periodicMotions.length; i++) {
        
        if (movieData.rows.length > 0) {
            let movieRow = movieData.rows[Math.floor(
                            millis() / updateTimeInterval) % movieData.rows.length];
            
            if (movieRow) {
                let movieObject = movieRow.obj;
                
                periodicMotions[i].update();
                rotate(movieObject['Duration']);
                
                periodicMotions[i].display(movieObject);
            }
        }
    }

    // movie name
    if (movieData.rows.length > 0) {
        var movieRow = movieData.rows[Math.floor(
                        millis() / updateTimeInterval) % movieData.rows.length];

        if (movieRow) {
            var movieObject = movieRow.obj;

            // CHECK IF THERE IS AN ENGLISH NAME, IF NOT DISPLAY RUSSIAN
            var movieNameEng = movieObject['NameEng'];
            var movieNameRus = movieObject['NameRus'];

            console.log('--------------');
            console.log('English Name:', movieNameEng);
            console.log('Russian Name:', movieNameRus);
            console.log('Duration:', movieObject['Duration']);
            
            if (!movieNameEng) {
                movieNameEng = movieNameRus;
            }

            pop();

            textSize(20);
            textAlign(CENTER);
            text(movieNameEng, width / 2, height / 2);
        }
    }
}

